#!/bin/bash

# Create a kubernetes cluster with up to 3 worker nodes and CVMFS enabled on CERN's OpenStack infrastructure 
# Can take up to 1h
openstack coe cluster create lodesman \
    --keypair lcgapp \
    --cluster-template kubernetes-1.15.3-1 \
    --master-flavor m2.medium \
    --flavor m2.xlarge \
    --labels auto_scaling_enabled=true \
    --labels min_node_count=1 \
    --labels max_node_count=3 \
    --labels admission_control_list=NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota,Priority \
    --labels cephfs_csi_enabled=true \
    --labels cephfs_csi_version=cern-csi-1.0-1 \
    --labels cern_enabled=true \
    --labels cgroup_driver=cgroupfs \
    --labels container_infra_prefix=gitlab-registry.cern.ch/cloud/atomic-system-containers/ \
    --labels cvmfs_csi_enabled=true \
    --labels cvmfs_csi_version=v1.0.0 \
    --labels cvmfs_tag=qa \
    --labels flannel_backend=vxlan \
    --labels heat_container_agent_tag=stein-dev-2 \
    --labels influx_grafana_dashboard_enabled=True \
    --labels ingress_controller=traefik \
    --labels kube_tag=v1.15.3 \
    --labels kube_csi_enabled=true \
    --labels kube_csi_version=cern-csi-1.0-1 \
    --labels manila_enabled=true \
    --labels manila_version=v0.3.0 \
    --labels cloud_provider_tag=v1.15.0 \
    --labels autoscaler_tag=v1.15.2 \
    --labels volume-driver=cinder\
