module lodesman

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	k8s.io/api v0.0.0-20191102065807-b98ecd433b91
	k8s.io/apimachinery v0.0.0-20191102025618-50aa20a7b23f
	k8s.io/client-go v0.0.0-20191016110837-54936ba21026
	k8s.io/utils v0.0.0-20191030222137-2b95a09bc58d // indirect
)
