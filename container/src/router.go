/* 
The router file contains the specification and implementation of the offered REST API.
*/

package main

import (
  "io"
  "fmt"
  "strings"
  "net/http"
  "io/ioutil"
  "encoding/json"
  "github.com/gorilla/mux"
)

// build_id_var_name ties the url var to the var used in the corresponding get function
var job_id_var_name = "job_id"

// initRouter creates a new mux router an associates the API endpoints
func initRouter() http.Handler {
  // Create router for REST API
  router := mux.NewRouter()
  // Add API endpoints and assign handlers
  router.HandleFunc("/helloworld", baseGetHandler).Methods("GET")
  router.HandleFunc(strings.Join([]string{"/status/", "{", job_id_var_name, "}", "/"}, ""), getStatusHandler).Methods("GET")
  router.HandleFunc(strings.Join([]string{"/logs/", "{", job_id_var_name, "}", "/"}, ""), getLogsHandler).Methods("GET")
  router.HandleFunc("/build", recvBuildJob).Methods("POST")
  return router
}

// baseGetHandler is used to test that the API is up.
func baseGetHandler(w http.ResponseWriter, r *http.Request) {
  //w.Header().Set("Content-Type", "applicaton/json")
  json.NewEncoder(w).Encode("HelloWorld")
}

// getStatus returns the status of a given job for the Jenkins progress indicator
func getStatusHandler(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  fmt.Println("Received request to get status of job %s.", vars[job_id_var_name])
  status := getStatus(vars[job_id_var_name])

  //w.Header().Set("Content-Type", "applicaton/json")
  json.NewEncoder(w).Encode(status.toString())
}

// getLogsHandler returns all logs for a given job
func getLogsHandler(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  fmt.Println("Received request to get logs of job %s", vars[job_id_var_name])
  logs := getLogs(vars[job_id_var_name])

  //w.Header().Set("Content-Type", "applicaton/json")
  json.NewEncoder(w).Encode(logs.toString())
}

// recvBuildJob handles post requests to the build job endpoint.
// Expects the request to have a JSON formatted body.
func recvBuildJob(w http.ResponseWriter, r *http.Request) {
  var job JenkinsJob
  // Read body content safely, limit to 1MiB
  body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
  if err != nil {
    panic(err)
  }
  if err := r.Body.Close(); err != nil {
    panic(err)
  }
  // Unmarshal JSON, return error response if something broke
  if err := json.Unmarshal(body, &job); err != nil {
    w.Header().Set("Content-Type", "application/json; charset=UTF-8")
    w.WriteHeader(http.StatusUnprocessableEntity)
    if err := json.NewEncoder(w).Encode (err); err != nil {
      panic(err)
    }
  }
  // Trigger job and return success response
  result := spawnBuildJob(job)
  //w.Header().Set("Content-Type", "application/json; charset=UTF-8")
  w.WriteHeader(http.StatusCreated)
  json.NewEncoder(w).Encode(result.GetName())
  if err := json.NewEncoder(w).Encode(job); err != nil {
    panic(err)
  }
  fmt.Println(job) 
}

