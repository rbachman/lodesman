/*
Package lodesman provides a HTTP-based REST API for Jenkins to queue its jobs on Kubernetes clusters.
It is designed to run as a persistent pod on the cluster in question.

Lodesman has been tailored to the needs of the CERN EP-SFT SPI group.
*/

package main

import (
  "io"
  "fmt"
  "time"
  "bytes"
  "strconv"
  "net/http"
  "k8s.io/client-go/rest"
  corev1 "k8s.io/api/core/v1"
  batchv1 "k8s.io/api/batch/v1"
  "k8s.io/client-go/kubernetes"
  "k8s.io/apimachinery/pkg/labels"
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var namespace = "lodesman"
var clientset kubernetes.Clientset

func createClientset() kubernetes.Clientset {
  config, err := rest.InClusterConfig()
  if err != nil {
    panic(err.Error())
  }
  clientset, err := kubernetes.NewForConfig(config)
  if err != nil {
    panic(err.Error())
  }
  return *clientset
}

// getStatus summarizes the job's (+pods) status for return to Jenkins
func getStatus(name string) JobStatus {
  // Get k8s job and its child(ren)
  jobsClient := clientset.BatchV1().Jobs(namespace)
  job, err := jobsClient.Get(name, metav1.GetOptions{})
  if err != nil {
    panic(err.Error())
  }

  fmt.Println("Found job: %s", job.Name)

  labelMap, err:= metav1.LabelSelectorAsMap(job.Spec.Selector)
  if err != nil {
    panic(err.Error())
  }

  fmt.Println("Using selector: %s", labelMap)

  podsClient := clientset.CoreV1().Pods(namespace)
  labelFilter := metav1.ListOptions{
    LabelSelector: labels.SelectorFromSet(labelMap).String(),
  }
  childList, err := podsClient.List(labelFilter)
  if err != nil {
    panic(err.Error())
  }

  // Get status of first child only for now
  pod := childList.Items[0]
  return JobStatus{
    pod.Name,
    string(pod.Status.Phase),
    string(pod.Status.Conditions[len(pod.Status.Conditions)-1].Type),
    pod.Status.Conditions[len(pod.Status.Conditions)-1].Message,
    pod.Status.Conditions[len(pod.Status.Conditions)-1].Reason,
  }
}

func getLogs(name string) JobLog {
  // Get k8s job and its child(ren)
  jobsClient := clientset.BatchV1().Jobs(namespace)
  job, err := jobsClient.Get(name, metav1.GetOptions{})
  if err != nil {
    panic(err.Error())
  }

  fmt.Print("Found job ")
  fmt.Println(job.Name)

  labelMap, err:= metav1.LabelSelectorAsMap(job.Spec.Selector)
  if err != nil {
    panic(err.Error())
  }

  fmt.Print("Using selector ")
  fmt.Println(labelMap)

  podsClient := clientset.CoreV1().Pods(namespace)
  labelFilter := metav1.ListOptions{
    LabelSelector: labels.SelectorFromSet(labelMap).String(),
  }
  childList, err := podsClient.List(labelFilter)
  if err != nil {
    panic(err.Error())
  }

  // Get logs from first child
  pod := childList.Items[0]

  podLogOptions := corev1.PodLogOptions{}
  logRequest := clientset.CoreV1().Pods(namespace).GetLogs(pod.Name, &podLogOptions)
  podLogs, err := logRequest.Stream()
  if err != nil {
    panic(err.Error())
  }
  defer podLogs.Close()

  buf := new(bytes.Buffer)
  _, err = io.Copy(buf, podLogs)
  if err != nil {
    panic(err.Error())
  }
  logStr := buf.String() // TODO: Do we need this step?
  timestamp := strconv.FormatInt(time.Now().Unix(), 10)
  return JobLog{
    pod.Name, 
    timestamp, 
    logStr,
  }
}

// spawnBuildJob interacts with the k8s API to create the job requested by Jenkins.
func spawnBuildJob(j JenkinsJob) *batchv1.Job {
  jobsClient := clientset.BatchV1().Jobs(namespace)

  // Produce job template
  job := makeJob(j)

  // Deploy pod to cluster
  fmt.Println("Creating job")
  result, err := jobsClient.Create(job)
  if err != nil {
    panic(err.Error())
  }
  fmt.Println("Created pod %q.\n", result.Name)
  return result
}

func main() {
  fmt.Println("Application run")
  router := initRouter()
  clientset = createClientset()
  fmt.Println("Listening and serving on local port 8000")
  http.ListenAndServe(":8000", router)
}
