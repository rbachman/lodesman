/*
The objects file contains introduced structs and their methods. 
*/

package main

import (
  "strings"
)

/* Data transport objects 
------------------------------------------------------------------------------*/

type JenkinsJob struct {
  Name               string `json:"name"`
  Timestamp          string `json:"timestamp"`
  LCG_Install_Prefix string `json:"lcg_install_prefix"`
  Version_Main       string `json:"version_main"`
  Target             string `json:"target"`
  LCG_Extra_Options  string `json:"lcg_extra_options"`
  LCG_Version        string `json:"lcg_version"`
  Compiler           string `json:"compiler"`
  Build_Type         string `json:"build_type"`
  OS                 string `json:"os"`
  Build_Mode         string `json:"build_mode"`
  Workspace          string `json:"workspace"`
  CTest_Track        string `json:"ctest_track"`
}

type JobStatus struct {
  Name               string `json:"name"`
  PodPhase           string `json:"pod_phase"`
  Type               string `json:"string"`
  Message            string `json:"message"`
  Reason             string `json:"reason"`
}

func (s *JobStatus) toString() string {
  return strings.Join([]string{s.Name, s.PodPhase, s.Type, s.Message, s.Reason}, ",   ")
}

type JobLog struct {
  Name               string `json:"name"`
  Timestamp          string `json:"timestamp"`
  Logs               string `json:"logs"`  
}

func (l *JobLog) toString() string {
  return l.Logs
}

