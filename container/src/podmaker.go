/*
The factory file contains templates and logic to create k8s objects.
*/

package main


import (
  "strings"
  apiv1 "k8s.io/api/core/v1"
  batchv1 "k8s.io/api/batch/v1"
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  resource "k8s.io/apimachinery/pkg/api/resource"
)

// TODO: Split into managable/modular chunks
// makeJob returns a k8s batch job with the requested specifications
func makeJob(job JenkinsJob) *batchv1.Job {
//  dockerRepoPrefix := "gitlab-registry.cern.ch/sft/docker/"
  dockerRepoPrefix := "gitlab-registry.cern.ch/rbachman/dockerhub/"
  image := strings.Join([]string{dockerRepoPrefix, job.OS, "_aedifex"},"")

  var activeDeadlineSeconds int64 = 24*60*60  // Complete within 24h
  var backoffLimit int32 = 6                  // Stop after 6 retries
  var ttlSecondsAfterFinished int32 = 30*60   // Keep pod 30m after finish

  // Perform resource limit calculations first, in case they fail
  mem_max, err := resource.ParseQuantity("14G")
  if err != nil {
    panic(err.Error())
  }

  mem_req, err := resource.ParseQuantity("10G")
  if err != nil {
    panic(err.Error())
  }

  var cpu_max int64 = 7000  // milliCPUs
  var cpu_req int64 = 4000  // milliCPUs

  return &batchv1.Job {
    ObjectMeta: metav1.ObjectMeta{
      Name: strings.Join([]string{job.Name, job.LCG_Version, job.Compiler, strings.ToLower(job.Build_Type), job.Timestamp}, "-"),
      Namespace: namespace,
    },
    Spec: batchv1.JobSpec{
      ActiveDeadlineSeconds: &activeDeadlineSeconds,
      BackoffLimit: &backoffLimit,
      TTLSecondsAfterFinished: &ttlSecondsAfterFinished,
      Template: apiv1.PodTemplateSpec{
        ObjectMeta: metav1.ObjectMeta{
          Name: job.Timestamp,
        },
        Spec: apiv1.PodSpec {
          Containers: []apiv1.Container{
            {
              Name: job.OS,
              Image: image,
              WorkingDir: job.Workspace,
              //Command: []string{"/bin/bash", "-c", "for i in {1..6}; do echo $i && sleep 10; done"},
              Command: []string{"/bin/bash", "-c", "/aedifex/build/build.sh"},
              Env: []apiv1.EnvVar{
                {
                  Name: "ADDITIONAL_CMAKE_OPTIONS",
                  Value: job.LCG_Extra_Options,
                },
                {
                  Name: "BUILD_MODE",
                  Value: job.Build_Mode,
                },
                {
                  Name: "BUILD_TYPE",
                  Value: job.Build_Type,
                },
                {
                  Name: "CTEST_TRACK",
                  Value: job.CTest_Track,
                },
                {
                  Name: "COMPILER",
                  Value: job.Compiler,
                },
                {
                  Name: "LCG_INSTALL_PREFIX",
                  Value: job.LCG_Install_Prefix,
                },
                {
                  Name: "LCG_VERSION",
                  Value: job.LCG_Version,
                },
                {
                  Name: "TARGET",
                  Value: job.Target,
                },
                {
                  Name: "USER",
                  Value: "sftnight",
                },
                {
                  Name: "WORKSPACE",
                  Value: job.Workspace,
                },
              },
              Resources: apiv1.ResourceRequirements{
                Limits: apiv1.ResourceList{
                  "cpu":*resource.NewMilliQuantity(cpu_max, resource.DecimalSI),
                  "memory":mem_max,
                },
                Requests: apiv1.ResourceList{
                  "cpu":*resource.NewMilliQuantity(cpu_req, resource.DecimalSI),
                  "memory":mem_req,
                },
              },
              VolumeMounts: []apiv1.VolumeMount{
                {
                  Name: "sft-pvc",
                  ReadOnly: true,
                  MountPath: "/cvmfs/sft.cern.ch",
                },
                {
                  Name: "workspace",
                  MountPath: job.Workspace,
                },
              },
            },
          },
          RestartPolicy: apiv1.RestartPolicyOnFailure,
          Volumes: []apiv1.Volume{
            {
              Name: "sft-pvc",
              VolumeSource: apiv1.VolumeSource{
                PersistentVolumeClaim: &apiv1.PersistentVolumeClaimVolumeSource{
                  ClaimName: "csi-cvmfs-sft-pvc",
                  ReadOnly: true,
                },
              },
            },
            {
              Name: "workspace",
              VolumeSource: apiv1.VolumeSource{
                PersistentVolumeClaim: &apiv1.PersistentVolumeClaimVolumeSource{
                  ClaimName: "workspace-standard",
                  ReadOnly: false,
                },
              },
            },
          },
        },
      },
    },
  }
}
