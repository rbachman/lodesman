# lodesman
A Go-based Jenkins worker middleman for an auto-scaling Kubernetes cluster.

## What will be here:

* A Go container with a REST API which makes use of client-go to utilize k8s.
* k8s definitions which enable this, such as service account, roles, service, ingress, etc.
* Sample Jenkins scripts to show how how Lodesman can be used

## The API
Lodesman currently has the following endpoints available:
```
/helloworld 
  └─ GET: Return helloworld string for testing
/build
  ├─ GET: Retrieve a list of queued build jobs. [Not yet implemented]
  └─ POST: Queue a build job. Expects parameters as json object.
/upload
  └─ POST: Upload build artefacts to EOS. [Not yet implemented]
/deploy
  └─ POST: Deploy artefacts to CVMFS. [Not yet implemented]
/test
  └─ POST: Test a deployment and upload the results to CDASH. [Not yet implemented]
/status/{job_id}
  └─ GET: Retrieve a summary of the job's and associated pod's status. 
/logs/{job_id}
  └─ GET: Retrieve the logs of a job and its associated pod.
/meta/
  ├─ logs/
  │   └─ GET: Get the Lodesman pod's logs. [Not yet implemented]
  └─ status/
      └─ GET: Get the Lodesman pod's status. [Not yet implemented]
```

## Troubleshooting and tricks
### Ingress on CERN openstack
In order to use the included ingress setup it is necessary to manually [label k8s nodes as ingress nodes](https://clouddocs.web.cern.ch/containers/tutorials/lb.html). 
To do this, find a set of minion nodes, usually the ones which are active when the cluster is scaled down to its minimum. Label them with `kubectl label node <node-id> role=ingress`.

## Stretch goals?

* Helm chart, or another way to deploy everything easily
